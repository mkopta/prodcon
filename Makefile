CC=gcc
CFLAGS=-ggdb -Wall -Wextra -pedantic -std=c11

all: read fread loop threads cqueue bulk consumers split producers

read: read.c
	$(CC) $(CFLAGS) -o read read.c

fread: fread.c
	$(CC) $(CFLAGS) -o fread fread.c

loop: loop.c
	$(CC) $(CFLAGS) -lm -o loop loop.c

threads: threads.c
	$(CC) $(CFLAGS) -lm -lpthread -o threads threads.c

cqueue: cqueue.c
	$(CC) $(CFLAGS) -lm -lpthread -o cqueue cqueue.c

bulk: bulk.c
	$(CC) $(CFLAGS) -lm -lpthread -o bulk bulk.c

consumers: consumers.c
	$(CC) $(CFLAGS) -lm -lpthread -o consumers consumers.c

split: split.c
	$(CC) $(CFLAGS) -lm -lpthread -o split split.c

producers: producers.c
	$(CC) $(CFLAGS) -lm -lpthread -o producers producers.c

clean:
	rm -f *.core read fread loop threads cqueue bulk consumers split producers
