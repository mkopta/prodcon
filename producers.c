/*
Multiple threads exchanging bulks of numbers
via fixed-size circular queue.
Multiple producers, multiple consumers.
*/
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>
#include <pthread.h>

#define CQUEUE_SIZE 4096
#define BULK_SIZE 1024
#define DATA_FILE "data.txt"

typedef struct cqueue {
    unsigned int size;
    unsigned int start;
    unsigned int end;
    bool is_full;
    void **items;
} cqueue_t;

typedef struct bulk {
    unsigned int size;
    double *numbers;
} bulk_t;

typedef struct args {
    FILE *fp;
    unsigned long stop_at;
    cqueue_t *q;
} args_t;

int running = 1;
pthread_mutex_t qlock;
pthread_cond_t consumed;
pthread_cond_t produced;
pthread_mutex_t qlock;
pthread_mutex_t collect;
unsigned long total = 0, count = 0;

bulk_t *bulk_create(unsigned int size) {
    bulk_t *b = (bulk_t *) malloc(sizeof(bulk_t));
    if (!b) {
        perror("malloc");
        exit(1);
    }
    b->size = size;
    b->numbers = (double *) malloc(size * sizeof(double));
    if (!b->numbers) {
        perror("malloc");
        exit(1);
    }
    return b;
}

void bulk_destroy(bulk_t *b) {
    free(b->numbers);
    free(b);
}

cqueue_t *cqueue_create(unsigned int size) {
    cqueue_t *q = malloc(sizeof(cqueue_t));
    if (!q) {
        perror("malloc");
        return NULL;
    }
    q->items = (void **) malloc(size * sizeof(void *));
    if (!q->items) {
        perror("malloc");
        free(q);
        return NULL;
    }
    q->size = size;
    q->start = 0;
    q->end = 0;
    q->is_full = false;
    return q;
}

void cqueue_destroy(cqueue_t *q) {
    if (q) {
        free(q->items);
        free(q);
    }
}

bool cqueue_full(cqueue_t *q) {
    return q->is_full;
}

bool cqueue_empty(cqueue_t *q) {
    return (bool) (q->start == q->end && !q->is_full);
}

bool cqueue_enqueue(cqueue_t *q, void *data) {
    if (q->is_full)
        return false;
    q->items[q->end] = data;
    q->end = (q->end + 1) % q->size;
    if (q->start == q->end)
        q->is_full = true;
    return true;
}

bool cqueue_dequeue(cqueue_t *q, void **data) {
    if (cqueue_empty(q))
        return false;
    *data = q->items[q->start];
    q->start = (q->start + 1) % q->size;
    if (q->is_full)
        q->is_full = false;
    return true;
}

bulk_t *dq_bulk(cqueue_t *q) {
    bulk_t *b;
    pthread_mutex_lock(&qlock);
    while (cqueue_empty(q)) {
        if (!running) {
            pthread_mutex_unlock(&qlock);
            return NULL;
        }
        pthread_cond_wait(&produced, &qlock);
    }
    cqueue_dequeue(q, (void **) &b);
    pthread_cond_signal(&consumed);
    pthread_mutex_unlock(&qlock);
    return b;
}

void nq_bulk(cqueue_t *q, bulk_t *b) {
    pthread_mutex_lock(&qlock);
    while (cqueue_full(q))
        pthread_cond_wait(&consumed, &qlock);
    cqueue_enqueue(q, (void *) b);
    pthread_cond_broadcast(&produced);
    pthread_mutex_unlock(&qlock);
}

void *consumer(void *_q) {
    unsigned int i;
    unsigned long local_total = 0, local_count = 0;
    double n;
    cqueue_t *q = (cqueue_t *) _q;
    bulk_t *b;
    while (true) {
        b = dq_bulk(q);
        if (!b)
            break;
        for (i = 0; i < b->size; i++) {
            n = b->numbers[i];
            n = round(exp(tan(log(tan(exp(tan(log(n))))))));
            local_total += (unsigned int) n;
        }
        local_count += b->size;
        bulk_destroy(b);
    }
    pthread_mutex_lock(&collect);
    total += local_total;
    count += local_count;
    pthread_mutex_unlock(&collect);
    return NULL;
}

void *producer(void *_args) {
    FILE *fp = ((args_t *) _args)->fp;
    long unsigned stop_at = ((args_t *) _args)->stop_at;
    cqueue_t *q = ((args_t *) _args)->q;
    long unsigned local_count = 0;
    char buf[19];
    double n;
    unsigned int i = 0;
    bulk_t *b = bulk_create(BULK_SIZE);
    do {
        if (fread(&buf, 19, 1, fp) == 0)
            break;
        buf[17] = '\0';
        n = strtod(buf, NULL);
        b->numbers[i++] = n;
        if (i == BULK_SIZE) {
            nq_bulk(q, b);
            b = bulk_create(BULK_SIZE);
            i = 0;
        }
    } while (local_count != stop_at);
    if (i != 0) {
        b->size = i;
        nq_bulk(q, b);
        pthread_cond_broadcast(&produced);
    }
    return NULL;
}

int main() {
    cqueue_t *q = cqueue_create(CQUEUE_SIZE);
    FILE *fp1 = fopen(DATA_FILE, "r");
    FILE *fp2 = fopen(DATA_FILE, "r");
    unsigned long total_numbers;
    args_t args1, args2;
    pthread_t th_producer1, th_producer2, th_consumer1, th_consumer2;
    pthread_cond_init(&produced, NULL);
    pthread_cond_init(&consumed, NULL);
    pthread_mutex_init(&qlock, NULL);
    pthread_mutex_init(&collect, NULL);

    fseek(fp1, 0, SEEK_END);
    total_numbers = ((unsigned long) ftell(fp1)) / 19;
    fseek(fp1, 0, SEEK_SET);

    args1.fp = fp1;
    args1.stop_at = total_numbers / 2;
    args1.q = q;
    fseek(fp2, (total_numbers / 2) * 19, SEEK_SET);
    args2.fp = fp2;
    args2.stop_at = 0;
    args2.q = q;

    pthread_create(&th_producer1, NULL, &producer, (void *) &args1);
    pthread_create(&th_producer2, NULL, &producer, (void *) &args2);
    pthread_create(&th_consumer1, NULL, &consumer, (void *) q);
    pthread_create(&th_consumer2, NULL, &consumer, (void *) q);
    pthread_join(th_producer1, NULL);
    pthread_join(th_producer2, NULL);
    running = 0;
    pthread_cond_broadcast(&produced);
    pthread_join(th_consumer1, NULL);
    pthread_join(th_consumer2, NULL);

    pthread_mutex_destroy(&collect);
    pthread_mutex_destroy(&qlock);
    pthread_cond_destroy(&consumed);
    pthread_cond_destroy(&produced);
    cqueue_destroy(q);
    fclose(fp1);
    fclose(fp2);

    if (count) {
        printf("Total %lu\n", total);
        printf("Count %lu\n", count);
        printf("Average %lu\n", total / count);
    }
    return 0;
}
