/*
Multiple threads exchanging bulks of numbers
via fixed-size circular queue.
One producer, multiple consumers.
*/
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>
#include <pthread.h>

#define CQUEUE_SIZE 4096
#define BULK_SIZE 1024
#define DATA_FILE "data.txt"

struct cqueue {
    unsigned int size;
    unsigned int start;
    unsigned int end;
    bool is_full;
    void **items;
};

struct bulk {
    unsigned int size;
    double *numbers;
};

int running = 1;
pthread_cond_t consumed;
pthread_cond_t produced;
pthread_mutex_t qlock;
pthread_mutex_t collect;
unsigned long total = 0, count = 0;

struct bulk *bulk_create(unsigned int size) {
    struct bulk *b = (struct bulk *) malloc(sizeof(struct bulk));
    if (!b) {
        perror("malloc");
        exit(1);
    }
    b->size = size;
    b->numbers = (double *) malloc(size * sizeof(double));
    if (!b->numbers) {
        perror("malloc");
        exit(1);
    }
    return b;
}

void bulk_destroy(struct bulk *b) {
    free(b->numbers);
    free(b);
}

struct cqueue *cqueue_create(unsigned int size) {
    struct cqueue *q = malloc(sizeof(struct cqueue));
    if (!q) {
        perror("malloc");
        return NULL;
    }
    q->items = (void **) malloc(size * sizeof(void *));
    if (!q->items) {
        perror("malloc");
        free(q);
        return NULL;
    }
    q->size = size;
    q->start = 0;
    q->end = 0;
    q->is_full = false;
    return q;
}

void cqueue_destroy(struct cqueue *q) {
    if (q) {
        free(q->items);
        free(q);
    }
}

bool cqueue_full(struct cqueue *q) {
    return q->is_full;
}

bool cqueue_empty(struct cqueue *q) {
    return (bool) (q->start == q->end && !q->is_full);
}

bool cqueue_enqueue(struct cqueue *q, void *data) {
    if (q->is_full)
        return false;
    q->items[q->end] = data;
    q->end = (q->end + 1) % q->size;
    if (q->start == q->end)
        q->is_full = true;
    return true;
}

bool cqueue_dequeue(struct cqueue *q, void **data) {
    if (cqueue_empty(q))
        return false;
    *data = q->items[q->start];
    q->start = (q->start + 1) % q->size;
    if (q->is_full)
        q->is_full = false;
    return true;
}

struct bulk *dq_bulk(struct cqueue *q) {
    struct bulk *b;
    pthread_mutex_lock(&qlock);
    while (cqueue_empty(q)) {
        if (!running) {
            pthread_mutex_unlock(&qlock);
            return NULL;
        }
        pthread_cond_wait(&produced, &qlock);
    }
    cqueue_dequeue(q, (void **) &b);
    pthread_cond_signal(&consumed);
    pthread_mutex_unlock(&qlock);
    return b;
}

void *consumer(void *_q) {
    unsigned int i;
    unsigned long local_total = 0, local_count = 0;
    double n;
    struct cqueue *q = (struct cqueue *) _q;
    struct bulk *b;
    while (true) {
        b = dq_bulk(q);
        if (!b)
            break;
        for (i = 0; i < b->size; i++) {
            n = b->numbers[i];
            n = round(exp(tan(log(tan(exp(tan(log(n))))))));
            local_total += (unsigned int) n;
        }
        local_count += b->size;
        bulk_destroy(b);
    }
    pthread_mutex_lock(&collect);
    total += local_total;
    count += local_count;
    pthread_mutex_unlock(&collect);
    return NULL;
}

void nq_bulk(struct cqueue *q, struct bulk *b) {
    pthread_mutex_lock(&qlock);
    while (cqueue_full(q))
        pthread_cond_wait(&consumed, &qlock);
    cqueue_enqueue(q, (void *) b);
    pthread_cond_broadcast(&produced);
    pthread_mutex_unlock(&qlock);
}

void *producer(void *_q) {
    FILE *fp = fopen(DATA_FILE, "r");
    char buf[19];
    double n;
    unsigned int i = 0;
    struct cqueue *q = (struct cqueue *) _q;
    struct bulk *b = bulk_create(BULK_SIZE);
    while (fread(&buf, 1, 19, fp) != 0) {
        buf[17] = '\0';
        n = strtod(buf, NULL);
        b->numbers[i++] = n;
        if (i == BULK_SIZE) {
            nq_bulk(q, b);
            b = bulk_create(BULK_SIZE);
            i = 0;
        }
    }
    if (i != 0) {
        b->size = i;
        nq_bulk(q, b);
    }
    running = 0;
    pthread_cond_broadcast(&produced);
    fclose(fp);
    return NULL;
}

int main() {
    pthread_t th_producer, th_consumer1, th_consumer2, th_consumer3;
    struct cqueue *q = cqueue_create(CQUEUE_SIZE);
    if (!q) {
        printf("error when creating queue\n");
        return 1;
    }
    pthread_cond_init(&produced, NULL);
    pthread_cond_init(&consumed, NULL);
    pthread_mutex_init(&qlock, NULL);
    pthread_mutex_init(&collect, NULL);
    pthread_create(&th_producer, NULL, &producer, (void *) q);
    pthread_create(&th_consumer1, NULL, &consumer, (void *) q);
    pthread_create(&th_consumer2, NULL, &consumer, (void *) q);
    pthread_create(&th_consumer3, NULL, &consumer, (void *) q);
    pthread_join(th_producer, NULL);
    pthread_join(th_consumer1, NULL);
    pthread_join(th_consumer2, NULL);
    pthread_join(th_consumer3, NULL);
    pthread_cond_destroy(&produced);
    pthread_cond_destroy(&consumed);
    pthread_mutex_destroy(&qlock);
    pthread_mutex_destroy(&collect);
    cqueue_destroy(q);
    printf("Total %lu\n", total);
    printf("Count %lu\n", count);
    printf("Average %lu\n", total / count);
    return 0;
}
