/*
Simple read of the data file with read(2)
*/
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>

int main() {
    int fd = open("data.txt", O_RDONLY);
    long unsigned count = 0;
    char buf[19];
    while (read(fd, &buf, 19) != 0) {
        count += 1;
    }
    close(fd);
    printf("Count %lu\n", count);
    return 0;
}
