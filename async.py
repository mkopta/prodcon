#!/usr/bin/env python3
import asyncio
from math import exp, tan, log

bg_tasks = asyncio.Queue(maxsize=1000)

total, count, results = 0, 0, {}

async def calculator(n):
    global total, count, results
    n = float(n)
    n = round(exp(tan(log(tan(exp(tan(log(n))))))))
    count += 1
    total += n
    results[n] = results.get(n, 0) + 1


async def collector():
    while True:
        t = await bg_tasks.get()
        if t == 'END':
            break
        await t


async def reader():
    with open('data.txt', 'r') as f:
        for line in f:
            f = asyncio.ensure_future(calculator(line))
            await bg_tasks.put(f)
    await bg_tasks.put('END')


async def main():
    print('Started')
    r = asyncio.ensure_future(reader())
    c = asyncio.ensure_future(collector())
    await r
    await c
    print(f'Total {total}')
    print(f'Count {count}')
    print(f'Average {round(total / count)}')
    #print(f'Frequency {results}')
    print('Finished')


if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    loop.run_until_complete(main())
    loop.close()
