/*
Simple single-thread loop reading lines
one by one with calculation on-the-fly.
*/
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main() {
    FILE *fp = fopen("data.txt", "r");
    long unsigned total = 0;
    long unsigned count = 0;
    char buf[19];
    double n;
    while (fread(&buf, 19, 1, fp) != 0) {
        buf[17] = '\0';
        n = strtod(buf, NULL);
        n = round(exp(tan(log(tan(exp(tan(log(n))))))));
        total += (unsigned int) n;
        count += 1;
    }
    fclose(fp);
    printf("Total %lu\n", total);
    printf("Count %lu\n", count);
    printf("Average %lu\n", total / count);
    return 0;
}
