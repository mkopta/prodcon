/*
Two threads exchanging numbers
via fixed-size circular queue.
*/
#include <stdio.h>
#include <math.h>
#include <pthread.h>
#include <stdbool.h>
#include <stdlib.h>

struct cqueue {
    unsigned int size;
    unsigned int start;
    unsigned int end;
    bool is_full;
    double *numbers;
};

int calc_running = 1;
pthread_cond_t flag;
pthread_mutex_t lock;

struct cqueue *cqueue_create(unsigned int size) {
    struct cqueue *cq = malloc(sizeof(struct cqueue));
    if (!cq) {
        perror("malloc");
        return NULL;
    }
    cq->numbers = malloc(size * sizeof(double));
    if (!cq->numbers) {
        perror("malloc");
        free(cq);
        return NULL;
    }
    cq->size = size;
    cq->start = 0;
    cq->end = 0;
    cq->is_full = false;
    return cq;
}

void cqueue_destroy(struct cqueue *cq) {
    if (cq) {
        free(cq->numbers);
        free(cq);
    }
}

bool cqueue_full(struct cqueue *cq) {
    return cq->is_full;
}

bool cqueue_empty(struct cqueue *cq) {
    return (bool) (cq->start == cq->end && !cq->is_full);
}

bool cqueue_enqueue(struct cqueue *cq, double n) {
    if (cq->is_full)
        return false;
    cq->numbers[cq->end] = n;
    cq->end = (cq->end + 1) % cq->size;
    if (cq->start == cq->end)
        cq->is_full = true;
    return true;
}

bool cqueue_dequeue(struct cqueue *cq, double *n) {
    if (cqueue_empty(cq))
        return false;
    *n = cq->numbers[cq->start];
    cq->start = (cq->start + 1) % cq->size;
    if (cq->is_full)
        cq->is_full = false;
    return true;
}

void *calc(void *_cq) {
    long unsigned total = 0;
    long unsigned count = 0;
    double n;
    struct cqueue *cq = (struct cqueue *) _cq;
    while (true) {
        pthread_mutex_lock(&lock);
        while (cqueue_empty(cq)) {
            if (!calc_running) {
                pthread_mutex_unlock(&lock);
                goto finished;
            }
            pthread_cond_wait(&flag, &lock);
        }
        cqueue_dequeue(cq, &n);
        pthread_cond_signal(&flag);
        pthread_mutex_unlock(&lock);
        n = round(exp(tan(log(tan(exp(tan(log(n))))))));
        total += (unsigned int) n;
        count += 1;
    }
finished:
    printf("Total %lu\n", total);
    printf("Count %lu\n", count);
    printf("Average %lu\n", total / count);
    return NULL;
}

void *load(void *_cq) {
    FILE *fp = fopen("data.txt", "r");
    char buf[19];
    double n;
    struct cqueue *cq = (struct cqueue *) _cq;
    while (fread(&buf, 19, 1, fp) != 0) {
        buf[17] = '\0';
        n = strtod(buf, NULL);
        pthread_mutex_lock(&lock);
        while (cqueue_full(cq))
            pthread_cond_wait(&flag, &lock);
        cqueue_enqueue(cq, n);
        pthread_cond_signal(&flag);
        pthread_mutex_unlock(&lock);
    }
    calc_running = 0;
    pthread_cond_signal(&flag);
    fclose(fp);
    return NULL;
}

int main() {
    pthread_t th_read, th_calc;
    struct cqueue *cq = cqueue_create(8);
    if (!cq) {
        printf("error when creating queue\n");
        return 1;
    }
    pthread_cond_init(&flag, NULL);
    pthread_mutex_init(&lock, NULL);
    pthread_create(&th_read, NULL, &load, (void *) cq);
    pthread_create(&th_calc, NULL, &calc, (void *) cq);
    pthread_join(th_read, NULL);
    pthread_join(th_calc, NULL);
    pthread_cond_destroy(&flag);
    pthread_mutex_destroy(&lock);
    cqueue_destroy(cq);
    return 0;
}
