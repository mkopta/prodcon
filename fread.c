/*
Simple read of the data file with fread(3)
*/
#include <stdio.h>

int main() {
    FILE *fp = fopen("data.txt", "r");
    long unsigned count = 0;
    char buf[19];
    while (fread(&buf, 19, 1, fp) != 0) {
        count += 1;
    }
    fclose(fp);
    printf("Count %lu\n", count);
    return 0;
}
