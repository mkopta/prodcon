#!/usr/bin/env python
from math import exp, tan, log

count = 0
total = 0
results = {}

with open('data.txt', 'r') as f:
    for line in f:
        n = float(line)
        n = round(exp(tan(log(tan(exp(tan(log(n))))))))
        count += 1
        total += n
        results[n] = results.get(n, 0) + 1
print(f'Total {total}')
print(f'Count {count}')
print(f'Average {round(total / count)}')
print(f'Frequency {results.get(n, 0)}')
