from multiprocessing import Process, Queue
from math import exp, tan, log
import os
import sys


def read_file(queue):
    batch = []
    with open('data.txt') as f:
        for line in f:
            pass
            #batch.append(line)
            #if len(batch) > 1000:
                #queue.put(batch)
            #    batch = []
    #if batch:
    #    queue.put(batch)
    queue.put('DONE')
    queue.put('DONE')
    queue.put('DONE')


def proces_numbers(queue, result):
    t = 0
    c = 0
    while True:
        d = queue.get()
        if d == 'DONE':
            break
        for n in d:
            n = round(exp(tan(log(tan(exp(tan(log(float(n.strip())))))))))
            c += 1
            t += n
    result.put((t, c))


q = Queue()
result = Queue()

px = Process(target=read_file, args=(q,))
p1 = Process(target=proces_numbers, args=(q, result))
p2 = Process(target=proces_numbers, args=(q, result))
p3 = Process(target=proces_numbers, args=(q, result))

px.start()
p1.start()
p2.start()
p3.start()

px.join()
p1.join()
p2.join()
p3.join()

total = 0
count = 0
while not result.empty():
    tx, cx = result.get()
    total = total + tx
    count = count + cx
print('Total: {}'.format(total))
print('Count: {}'.format(count))
print('Av: {}'.format(total/count))
