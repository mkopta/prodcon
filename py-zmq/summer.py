#!/usr/bin/env python
import sys
import zmq

context = zmq.Context()
sock = context.socket(zmq.PULL)
for port in sys.argv[1:]:
    sock.connect(f'tcp://localhost:{port}')
results = []
print('Waiting for results')
while len(results) != len(sys.argv[1:]):
    msg = sock.recv_string()
    results.append(msg.split())
print('All results collected')
total, count = 0, 0
for partial_sum, partial_count in results:
    total += float(partial_sum)
    count += int(partial_count)
print('Average')
print(total / count)
