#!/usr/bin/env python
import sys
from math import exp, tan, log

import zmq

push_port = sys.argv[1]
context = zmq.Context()
sock = context.socket(zmq.PULL)
sock.connect('ipc:///tmp/reader')
count, total, results = 0, 0, {}
print('Receiving numbers')
while True:
    msg = sock.recv_string()
    if msg == 'stop':
        print('Stopping')
        break
    ns = msg.split(' ')
    for n in ns:
        n = round(exp(tan(log(tan(exp(tan(log(float(n)))))))))
        count += 1
        total += n
        results[n] = results.get(n, 0) + 1
print('Sending results')
context = zmq.Context()
sock = context.socket(zmq.PUSH)
sock.bind(f'tcp://*:{push_port}')
sock.send_string(f'{total} {count}')
print('Finished')
