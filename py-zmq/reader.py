#!/usr/bin/env python
import sys
import zmq

calculators = int(sys.argv[1])
context = zmq.Context()
sock = context.socket(zmq.PUSH)
sock.bind('ipc:///tmp/reader')
print('Sending numbers')
packet = []
with open('data.txt', 'r') as f:
    for line in f:
        packet.append(line.strip())
        if len(packet) == 1000:
            sock.send_string(' '.join(packet))
            packet = []
    if packet:
        sock.send_string(' '.join(packet))
print('Halting calculators')
while calculators:
    sock.send_string('stop')
    calculators -= 1
print('Finished')
