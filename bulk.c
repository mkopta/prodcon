/*
Two threads exchanging bulks of numbers
via fixed-size circular queue.
*/
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>
#include <pthread.h>

#define CQUEUE_SIZE 256
#define BULK_SIZE 1000
#define DATA_FILE "data.txt"

struct cqueue {
    unsigned int size;
    unsigned int start;
    unsigned int end;
    bool is_full;
    void **items;
};

struct bulk {
    unsigned int size;
    double *numbers;
};

int calc_running = 1;
pthread_cond_t flag;
pthread_mutex_t lock;

struct bulk *bulk_create(unsigned int size) {
    struct bulk *b = (struct bulk *) malloc(sizeof(struct bulk));
    if (!b) {
        perror("malloc");
        exit(1);
    }
    b->size = size;
    b->numbers = (double *) malloc(size * sizeof(double));
    if (!b->numbers) {
        perror("malloc");
        exit(1);
    }
    return b;
}

void bulk_destroy(struct bulk *b) {
    free(b->numbers);
    free(b);
}

struct cqueue *cqueue_create(unsigned int size) {
    struct cqueue *cq = malloc(sizeof(struct cqueue));
    if (!cq) {
        perror("malloc");
        return NULL;
    }
    cq->items = (void **) malloc(size * sizeof(void *));
    if (!cq->items) {
        perror("malloc");
        free(cq);
        return NULL;
    }
    cq->size = size;
    cq->start = 0;
    cq->end = 0;
    cq->is_full = false;
    return cq;
}

void cqueue_destroy(struct cqueue *cq) {
    if (cq) {
        free(cq->items);
        free(cq);
    }
}

bool cqueue_full(struct cqueue *cq) {
    return cq->is_full;
}

bool cqueue_empty(struct cqueue *cq) {
    return (bool) (cq->start == cq->end && !cq->is_full);
}

bool cqueue_enqueue(struct cqueue *cq, void *data) {
    if (cq->is_full)
        return false;
    cq->items[cq->end] = data;
    cq->end = (cq->end + 1) % cq->size;
    if (cq->start == cq->end)
        cq->is_full = true;
    return true;
}

bool cqueue_dequeue(struct cqueue *cq, void **data) {
    if (cqueue_empty(cq))
        return false;
    *data = cq->items[cq->start];
    cq->start = (cq->start + 1) % cq->size;
    if (cq->is_full)
        cq->is_full = false;
    return true;
}

struct bulk *dq_bulk(struct cqueue *cq) {
    struct bulk *b;
    pthread_mutex_lock(&lock);
    while (cqueue_empty(cq)) {
        if (!calc_running) {
            pthread_mutex_unlock(&lock);
            return NULL;
        }
        pthread_cond_wait(&flag, &lock);
    }
    cqueue_dequeue(cq, (void **) &b);
    pthread_cond_signal(&flag);
    pthread_mutex_unlock(&lock);
    return b;
}

void *calc(void *_cq) {
    unsigned int i;
    long unsigned total = 0, count = 0;
    double n;
    struct cqueue *cq = (struct cqueue *) _cq;
    struct bulk *b;
    while (true) {
        b = dq_bulk(cq);
        if (!b)
            break;
        for (i = 0; i < b->size; i++) {
            n = b->numbers[i];
            n = round(exp(tan(log(tan(exp(tan(log(n))))))));
            total += (unsigned int) n;
        }
        count += b->size;
        bulk_destroy(b);
    }
    printf("Total %lu\n", total);
    printf("Count %lu\n", count);
    printf("Average %lu\n", total / count);
    return NULL;
}

void nq_bulk(struct cqueue *cq, struct bulk *b) {
    pthread_mutex_lock(&lock);
    while (cqueue_full(cq))
        pthread_cond_wait(&flag, &lock);
    cqueue_enqueue(cq, (void *) b);
    pthread_cond_signal(&flag);
    pthread_mutex_unlock(&lock);
}

void *load(void *_cq) {
    FILE *fp = fopen(DATA_FILE, "r");
    char buf[19];
    double n;
    unsigned int i = 0;
    struct cqueue *cq = (struct cqueue *) _cq;
    struct bulk *b = bulk_create(BULK_SIZE);
    while (fread(&buf, 19, 1, fp) != 0) {
        buf[17] = '\0';
        n = strtod(buf, NULL);
        b->numbers[i++] = n;
        if (i == BULK_SIZE) {
            nq_bulk(cq, b);
            b = bulk_create(BULK_SIZE);
            i = 0;
        }
    }
    if (i != 0) {
        b->size = i;
        nq_bulk(cq, b);
    }
    calc_running = 0;
    pthread_cond_signal(&flag);
    fclose(fp);
    return NULL;
}

int main() {
    pthread_t th_read, th_calc;
    struct cqueue *cq = cqueue_create(CQUEUE_SIZE);
    if (!cq) {
        printf("error when creating queue\n");
        return 1;
    }
    pthread_cond_init(&flag, NULL);
    pthread_mutex_init(&lock, NULL);
    pthread_create(&th_read, NULL, &load, (void *) cq);
    pthread_create(&th_calc, NULL, &calc, (void *) cq);
    pthread_join(th_read, NULL);
    pthread_join(th_calc, NULL);
    pthread_cond_destroy(&flag);
    pthread_mutex_destroy(&lock);
    cqueue_destroy(cq);
    return 0;
}
