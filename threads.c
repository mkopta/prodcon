/*
Two threads:
- reader (producer)
- calculator (consumer).
Exchanging one number at a time.
*/
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <pthread.h>

int calc_running = 1;
int data_ready = 0;
pthread_cond_t flag;
pthread_mutex_t lock;

void *calc(void *handover) {
    long unsigned total = 0;
    long unsigned count = 0;
    double n;
    while (1) {
        pthread_mutex_lock(&lock);
        while (!data_ready) {
            if (!calc_running) {
                pthread_mutex_unlock(&lock);
                goto finished;
            }
            pthread_cond_wait(&flag, &lock);
        }
        n = *(double *)handover;
        data_ready = 0;
        pthread_cond_signal(&flag);
        pthread_mutex_unlock(&lock);
        n = round(exp(tan(log(tan(exp(tan(log(n))))))));
        total += (unsigned int) n;
        count += 1;
    }
finished:
    printf("Total %lu\n", total);
    printf("Count %lu\n", count);
    printf("Average %lu\n", total / count);
    return NULL;
}

void *load(void *handover) {
    FILE *fp = fopen("data.txt", "r");
    char buf[19];
    double n;
    while (fread(&buf, 19, 1, fp) != 0) {
        buf[17] = '\0';
        n = strtod(buf, NULL);
        pthread_mutex_lock(&lock);
        if (data_ready) {
            pthread_cond_wait(&flag, &lock);
        }
        *(double *)handover = n;
        data_ready = 1;
        pthread_cond_signal(&flag);
        pthread_mutex_unlock(&lock);
    }
    calc_running = 0;
    pthread_cond_signal(&flag);
    fclose(fp);
    return NULL;
}

int main() {
    pthread_t th_read, th_calc;
    double handover;
    pthread_cond_init(&flag, NULL);
    pthread_mutex_init(&lock, NULL);
    pthread_create(&th_read, NULL, &load, (void *) (void *) &handover);
    pthread_create(&th_calc, NULL, &calc, (void *) (void *) &handover);
    pthread_join(th_read, NULL);
    pthread_join(th_calc, NULL);
    pthread_cond_destroy(&flag);
    pthread_mutex_destroy(&lock);
    return 0;
}
