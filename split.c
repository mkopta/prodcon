/*
Reading the data file with two threads.
*/
#include <stdio.h>
#include <pthread.h>

typedef struct args {
    FILE *fp;
    unsigned long stop_at;
} args_t;

pthread_mutex_t lock;
unsigned long count = 0;

void *load(void *_args) {
    FILE *fp = ((args_t *) _args)->fp;
    long unsigned stop_at = ((args_t *) _args)->stop_at;
    long unsigned local_count = 0;
    char buf[19];
    do {
        if (fread(&buf, 19, 1, fp) == 0)
            break;
        local_count++;
    } while (local_count != stop_at);
    pthread_mutex_lock(&lock);
    count += local_count;
    pthread_mutex_unlock(&lock);
    return NULL;
}

int main() {
    FILE *fp1 = fopen("data.txt", "r");
    FILE *fp2 = fopen("data.txt", "r");
    unsigned long total_numbers;
    args_t args1, args2;
    pthread_t th_load1, th_load2;
    pthread_mutex_init(&lock, NULL);

    fseek(fp1, 0, SEEK_END);
    total_numbers = ((unsigned long) ftell(fp1)) / 19;
    fseek(fp1, 0, SEEK_SET);

    args1.fp = fp1;
    args1.stop_at = total_numbers / 2;
    fseek(fp2, (total_numbers / 2) * 19, SEEK_SET);
    args2.fp = fp2;
    args2.stop_at = 0;

    pthread_create(&th_load1, NULL, &load, (void *) &args1);
    pthread_create(&th_load2, NULL, &load, (void *) &args2);
    pthread_join(th_load1, NULL);
    pthread_join(th_load2, NULL);

    pthread_mutex_destroy(&lock);
    fclose(fp1);
    fclose(fp2);
    printf("Count %lu\n", count);
    return 0;
}
